﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using EonDream.SmartHome.TpLink;

namespace tplink.tests {
    [TestClass]
    public class SmartHomeProtocolTests {
        readonly byte[] expectedBytes = new byte[] {
                0,
                0,
                0,
                29,
                208,
                242,
                129,
                248,
                139,
                255,
                154,
                247,
                213,
                239,
                148,
                182,
                209,
                180,
                192,
                159,
                236,
                149,
                230,
                143,
                225,
                135,
                232,
                202,
                240,
                139,
                246,
                139,
                246
            };

        readonly string expectedString = "{\"system\":{\"get_sysinfo\":{}}}";


        [TestMethod]
        public void TestEncrypt() {
            var p = new SmartHomeProtocol("127.0.0.1");
            var returnValue = p.Encrypt(expectedString);
            CollectionAssert.AreEqual(expectedBytes, returnValue);
        }

        [TestMethod]
        public void TestDecrypt() {
            var p = new SmartHomeProtocol("127.0.0.1");
            var returnValue = p.Decrypt(expectedBytes, expectedBytes.Length);
            Assert.AreEqual(expectedString, returnValue);
        }

        [TestMethod]
        public void TestEncryptDecrypt() {
            string value = "this is a test. this is only a test. everything should work. {}!@#$%^&*()_+=-/.,;''][=-0123456789*-/+";
            var p = new SmartHomeProtocol("127.0.0.1");
            var encValue = p.Encrypt(value);
            var decValue = p.Decrypt(encValue, encValue.Length);
            Assert.AreEqual(value, decValue);
        }
    }
}
