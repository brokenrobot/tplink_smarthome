﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EonDream.SmartHome.TpLink {
    class Program {
        static void Main(string[] args) {
            var plug = new HS110("192.168.1.127");

            plug.SendingCommand += (o, e) => { Console.WriteLine($"[DEBUG - CMD] { e.CommandType } - { e.CommandName } : { e.DataJson }"); };
            plug.ReceivedResponse += (o, e) => { Console.WriteLine($"[DEBUG - RSP] - { e.CommandType } - { e.CommandName } : { e.DataJson }"); };

            var x = plug.ReadDeviceDetails();

            Console.WriteLine($"Connected to plug named: { x.Alias }");

            //plug.EnablePower();
            //plug.DisablePower();

            var pwr = plug.ReadElectricityMeter();
            Console.WriteLine($"Voltage: { pwr.Voltage }\nCurrent:{ pwr.Current}\nPower:{ pwr.Power}");
            var gain = plug.ReadElectricityGain();
            Console.WriteLine($"VGain: { gain.VGain }\nIGain: { gain.IGain }");

            //plug.GetTime();
            //plug.GetTimezone();



            Console.ReadLine();
        }
    }
}
