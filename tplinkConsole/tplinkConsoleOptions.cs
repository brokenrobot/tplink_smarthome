﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;

namespace EonDream.SmartHome.TpLink {
    class tplinkConsoleOptions {
        private const string CONNSTRING_NAME = "TPLINKDB";
        private const string CONFIG_DBNAME = "DATABASE_NAME";

        public string SqlConnectionString {
            get {
                return ConfigurationManager.ConnectionStrings[CONNSTRING_NAME].ConnectionString;
            }
        }

        public string DatabaseName {
            get {
                return ConfigurationManager.AppSettings[CONFIG_DBNAME];
            }
        }
    }
}
