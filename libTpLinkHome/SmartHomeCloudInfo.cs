﻿using System;
using System.Collections.Generic;
using System.Text;
using Newtonsoft.Json;

namespace EonDream.SmartHome.TpLink {
    public class SmartHomeCloudInfo : SmartHomeResponse {
        public string Username { get; set; }
        public string Server { get; set; }
        public int Binded { get; set; }
        public int CldConnection { get; set; }
        public int IllegalType { get; set; }
        public int TcspStatus { get; set; }
        [JsonProperty(PropertyName = "fwDlPage")]
        public string FirmwareDownloadUrl { get; set; }
        public string tcspInfo { get; set; }
        public int StopConnect { get; set; }
        [JsonProperty(PropertyName = "fwNotifyType")]
        public int FirmwareNotifyType { get; set; }
    }
}
