﻿using System;
using System.Collections.Generic;
using System.Text;
using Newtonsoft.Json;

namespace EonDream.SmartHome.TpLink {
    public class SmartHomeStats : SmartHomeResponse {
        [JsonProperty(PropertyName = "month_list")]
        public List<StatsMonth> Data { get; set; }
    }
}
