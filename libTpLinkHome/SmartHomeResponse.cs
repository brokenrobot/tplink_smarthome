﻿using System;
using System.Collections.Generic;
using System.Text;
using Newtonsoft.Json;

namespace EonDream.SmartHome.TpLink {
    public class SmartHomeResponse {
        /// <summary>
        /// Error code
        /// </summary>
        [JsonProperty(PropertyName = "err_code")]
        public int ErrorCode { get; set; }
    }
}