﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EonDream.SmartHome.TpLink {
    public class ElectricityGainSettings : SmartHomeResponse {
        public int VGain { get; set; }
        public int IGain { get; set; }
    }
}
