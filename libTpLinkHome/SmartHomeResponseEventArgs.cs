﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EonDream.SmartHome.TpLink {
    public class SmartHomeResponseEventArgs : EventArgs {
        public string CommandType { get; set; }
        public string CommandName { get; set; }
        public string DataJson { get; set; }
    }
}
