﻿using System;
using System.Collections.Generic;
using System.Text;
using Newtonsoft.Json;

namespace EonDream.SmartHome.TpLink {
    public class ElectricityReading : SmartHomeResponse {
        [JsonProperty(PropertyName = "voltage_mv")]
        public int Voltage { get; set; }
        [JsonProperty(PropertyName = "current_ma")]
        public int Current { get; set; }
        [JsonProperty(PropertyName = "power_mw")]
        public int Power { get; set; }
        [JsonProperty(PropertyName = "total_wh")]
        public int TotalWattHours { get; set; }
    }
}
