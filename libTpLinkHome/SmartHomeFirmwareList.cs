﻿using System;
using System.Collections.Generic;
using System.Text;
using Newtonsoft.Json;

namespace EonDream.SmartHome.TpLink {
    public class SmartHomeFirmwareList : SmartHomeResponse {
        [JsonProperty(PropertyName = "fw_list")]
        public IEnumerable<SmartHomeFirmwareInfo> Firmware { get; set; }
    }
}
