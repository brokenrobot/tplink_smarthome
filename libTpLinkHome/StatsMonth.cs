﻿using System;
using System.Collections.Generic;
using System.Text;
using Newtonsoft.Json;

namespace EonDream.SmartHome.TpLink {
    public class StatsMonth {
        public int Year { get; set; }
        public int Month { get; set; }
        [JsonProperty(PropertyName = "energy_wh")]
        public int Energy { get; set; }
    }
}
