﻿using System;
using System.Collections.Generic;
using System.Text;
using Newtonsoft.Json;

namespace EonDream.SmartHome.TpLink {
    public class SmartHomeWifiList : SmartHomeResponse {
        [JsonProperty(PropertyName = "ap_list")]
        public IEnumerable<SmartHomeWifiNetwork> Networks { get; set; }
    }
}
