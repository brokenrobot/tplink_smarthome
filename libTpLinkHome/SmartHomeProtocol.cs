﻿/*
 * A huge thanks to Lubomir Stroetmann for reverse engineering the protocol: https://www.softscheck.com/en/reverse-engineering-tp-link-hs110/
 * This code is based on their work in python: https://github.com/softScheck/tplink-smartplug
 */

using System;
using System.Collections.Generic;
using System.Net.Sockets;
using System.Threading.Tasks;
using System.Text;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace EonDream.SmartHome.TpLink {
    /// <summary>
    /// Client for the TP-Link smart home protocol
    /// </summary>
    public class SmartHomeProtocol {
        /// <summary>
        /// Gets or sets the IP address or hostname of the smart home device
        /// </summary>
        public string Address { get; set; }

        /// <summary>
        /// Gets or sets the port number to use for network communications. (Default is 9999)
        /// </summary>
        public int Port { get; set; } = 9999;

        /// <summary>
        /// Gets or sets the default buffer size to use for network communications. (Default is 2048)
        /// </summary>
        public int ReadBufferSize { get; set; } = 2048;

        /// <summary>
        /// Gets or sets the initial cypher key value to use when encrypting and decrypting data transmissions. (Default is 171)
        /// </summary>
        public int InitialCypherKeyValue { get; set; } = 171;
        /// <summary>
        /// Event is raised just before sending command data to the device.
        /// </summary>
        public event EventHandler<SmartHomeEventArgs> SendingCommand;

        /// <summary>
        /// Event is raised just after receiving a response from the device and before json deserialization.
        /// </summary>
        public event EventHandler<SmartHomeEventArgs> ReceivedResponse;

        /// <summary>
        /// Creates a new SmartHomeProtocol object
        /// </summary>
        /// <param name="Address">IP address or hostname of the smart home device</param>
        public SmartHomeProtocol(string Address) {
            this.Address = Address;
        }

        protected virtual void OnSendingCommand(string cmdType, string cmdName, string json) {
            var args = new SmartHomeEventArgs() {
                CommandType = cmdType,
                CommandName = cmdName,
                DataJson = json
            };
            SendingCommand?.Invoke(this, args);
        }

        protected virtual void OnRecievedResponse(string cmdType, string cmdName, string json) {
            var args = new SmartHomeEventArgs() {
                CommandType = cmdType,
                CommandName = cmdName,
                DataJson = json
            };
            ReceivedResponse?.Invoke(this, args);
        }

        /// <summary>
        /// Asynchronously sends a command to the smart home device and returns the response.
        /// </summary>
        /// <typeparam name="T">Type of the response object</typeparam>
        /// <param name="commandType">Command type</param>
        /// <param name="commandName">Command name</param>
        /// <param name="commandData">Key value pairs of parameters for the command.</param>
        /// <returns>An object of the response type</returns>
        public async Task<T> SendCommandAsync<T>(string commandType, string commandName, IDictionary<string, string> commandData) where T : class {
            var commandJson = GenerateCommandJson(commandType, commandName, ProcessCommandParameters(commandData));
            return await SendCommandAsync<T>(commandType, commandName, commandJson);
        }

        /// <summary>
        /// Asynchronously sends a command to the smart home device and returns the response.
        /// </summary>
        /// <typeparam name="T">Type of the response object</typeparam>
        /// <param name="commandType">Command type</param>
        /// <param name="commandName">Command name</param>
        /// <param name="commandData">An object containing command parameter data. (Optional; Default is null)</param>
        /// <returns>An object of the response type</returns>
        public async Task<T> SendCommandAsync<T>(string commandType, string commandName, object commandData = null) where T : class {
            var commandJson = GenerateCommandJson(commandType, commandName, ProcessCommandParameters(commandData));
            return await SendCommandAsync<T>(commandType, commandName, commandJson);
        }

        /// <summary>
        /// Asynchronously sends a command to the smart home device and returns the response.
        /// </summary>
        /// <typeparam name="T">Type of the response object</typeparam>
        /// <param name="commandType">Command type</param>
        /// <param name="commandName">Command name</param>
        /// <param name="commandJson">JSON of the command parameter data.</param>
        /// <returns>An object of the response type</returns>
        public async Task<T> SendCommandAsync<T>(string commandType, string commandName, string commandJson) where T : class {
            // this is the place to add core logic to the command sending process.
            // all other SendCommand and SendCommandAsync methods end up here eventually.

            OnSendingCommand(commandType, commandName, commandJson);

            var commandResponseJson = await SendDataAsync(commandJson);

            OnRecievedResponse(commandType, commandName, commandJson);

            var responseWrapper = JObject.Parse(commandResponseJson);
            return responseWrapper[commandType][commandName].ToObject<T>();
        }

        /// <summary>
        /// Sends a command to the smart home device and returns the response.
        /// </summary>
        /// <typeparam name="T">Type of the response object</typeparam>
        /// <param name="commandType">Command type</param>
        /// <param name="commandName">Command name</param>
        /// <param name="commandData">Key value pairs of parameters for the command.</param>
        /// <returns>An object of the response type</returns>
        public T SendCommand<T>(string commandType, string commandName, IDictionary<string, string> commandData) where T : class {
            return SendCommandAsync<T>(commandType, commandName, commandData).Result;
        }

        /// <summary>
        /// Sends a command to the smart home device and returns the response.
        /// </summary>
        /// <typeparam name="T">Type of the response object</typeparam>
        /// <param name="commandType">Command type</param>
        /// <param name="commandName">Command name</param>
        /// <param name="commandData">An object containing command parameter data. (Optional; Default is null)</param>
        /// <returns>An object of the response type</returns>
        public T SendCommand<T>(string commandType, string commandName, object commandData = null) where T : class {
            return SendCommandAsync<T>(commandType, commandName, commandData).Result;
        }

        /// <summary>
        /// Processes command parameters in a dictionary collection
        /// </summary>
        /// <param name="commandData">Key value pairs of parameters for the command.</param>
        /// <returns>JSON object of the command parameter data</returns>
        private JObject ProcessCommandParameters(IDictionary<string, string> parameters) {
            if (parameters == null) {
                // no parameters
                return null;
            }
            else {
                // create data json object
                var data = new JObject();

                // proces the data types for the parameters in the command data and add them to the json object
                foreach (var param in parameters) {
                    int isInt = 0;
                    if (int.TryParse(param.Value, out isInt)) {
                        data.Add(new JProperty(param.Key, isInt));
                    }
                    else {
                        data.Add(new JProperty(param.Key, param.Value));
                    }
                }

                return data;
            }
        }

        /// <summary>
        /// Process an object containing command parameter data
        /// </summary>
        /// <param name="parameterData">An object containing command parameter data. (Optional; Default is null)</param>
        /// <returns>JSON object of the command parameter data</returns>
        private JObject ProcessCommandParameters(object parameterData) {
            if (parameterData == null) {
                return null;
            }
            else {
                return JObject.FromObject(parameterData);
            }
        }

        /// <summary>
        /// Generates the JSON data that is sent to the device
        /// </summary>
        /// <param name="commandType">Command type</param>
        /// <param name="commandName">Command name</param>
        /// <param name="commandData">Key value pairs of parameters for the command. (Optional; Default is null)</param>
        /// <returns>JSON string that defines the command to be executed by the smart home device</returns>
        public string GenerateCommandJson(string commandType, string commandName, JObject commandParameters) {
            var cmd = new JObject();
            cmd.Add(commandName, commandParameters);

            var wrapper = new JObject();
            wrapper.Add(commandType, cmd);
            return wrapper.ToString(Formatting.None);
        }

        /// <summary>
        /// Asynchronously sends a string of data to the smart home device and returns the response string.
        /// </summary>
        /// <param name="data">JSON data command string</param>
        /// <returns>JSON data response string</returns>
        public async Task<string> SendDataAsync(string data) {
            string response = null;

            using (var tcp = new TcpClient()) {
                await tcp.ConnectAsync(Address, Port);
                using (var netStream = tcp.GetStream()) {
                    var dataBytes = Encrypt(data);
                    await netStream.WriteAsync(dataBytes, 0, dataBytes.Length);
                    var buffer = new byte[ReadBufferSize];
                    await netStream.ReadAsync(buffer, 0, buffer.Length);
                    response = Decrypt(buffer, buffer.Length);
                }
            }

            return response;
        }

        /// <summary>
        /// Encrypts data before being transmitted to the smart home device
        /// </summary>
        /// <param name="data">Data to be encrypted</param>
        /// <returns>Encrypted byte array</returns>
        public byte[] Encrypt(string data) {
            /*
             * 	key = 171
                result = "\0\0\0\0"
                for i in string: 
                    a = key ^ ord(i)
                    key = a
                    result += chr(a)
                return result
            */
            var result = new List<byte>() { 0, 0, 0, (byte)data.Length };

            var key = InitialCypherKeyValue;

            foreach (int dataChar in data) {
                var a = key ^ dataChar;
                key = a;
                result.Add((byte)a);
            }

            return result.ToArray();
        }

        /// <summary>
        /// Decrypts data sent back from the smart home device
        /// </summary>
        /// <param name="data">Encrypted byte array</param>
        /// <param name="bufferLength">Length of buffer array</param>
        /// <returns>Decrypted string</returns>
        public string Decrypt(byte[] data, int bufferLength) {
            /*
             * def decrypt(string):
	key = 171 
	result = ""
	for i in string: 
		a = key ^ ord(i)
		key = ord(i) 
		result += chr(a)
	return result
    */
            var key = InitialCypherKeyValue;

            var result = new StringBuilder();
            for (int i = 4; i < bufferLength; i++) {
                if (data[i] == 0) {
                    break;
                }

                var a = key ^ data[i];
                key = data[i];

                result.Append((char)a);
            }
            return result.ToString();
        }
    }
}
