﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Text;

namespace EonDream.SmartHome.TpLink {
    /// <summary>
    /// Works with the tp-link HS110 smart plug
    /// </summary>
    public class HS110 {
        /// <summary>
        /// Event is raised just before sending command data to the device.
        /// </summary>
        public event EventHandler<SmartHomeEventArgs> SendingCommand;

        /// <summary>
        /// Event is raised just after receiving a response from the device and before json deserialization.
        /// </summary>
        public event EventHandler<SmartHomeEventArgs> ReceivedResponse;

        private SmartHomeProtocol client;
        public HS110(string Address) {
            client = new SmartHomeProtocol(Address);
            client.SendingCommand += (o, e) => { OnSendingCommand(e); };
            client.ReceivedResponse += (o, e) => { OnReceivedResponse(e); };
        }

        protected virtual void OnSendingCommand(SmartHomeEventArgs args) {
            SendingCommand?.Invoke(this, args);
        }

        protected virtual void OnReceivedResponse(SmartHomeEventArgs args) {
            ReceivedResponse?.Invoke(this, args);
        }

        /// <summary>
        /// Reads the details of the smart home device.
        /// </summary>
        /// <returns>Details of the TP-LINK HS100/HS110 smart home device</returns>
        public SmartHomeDeviceInfo ReadDeviceDetails() {
            return client.SendCommand<SmartHomeDeviceInfo>("system", "get_sysinfo");
        }

        /// <summary>
        /// Reboots the device
        /// </summary>
        /// <param name="delay">Delay reboot (Default is 1)</param>
        public void Reboot(int delay = 1) {
            var data = new Dictionary<string, string>() {
                { "delay", delay.ToString() }
            };
            client.SendCommand<SmartHomeResponse>("system", "reboot", data);
        }

        /// <summary>
        /// Sets the device's display name
        /// </summary>
        /// <param name="newAlias">New display name</param>
        public void SetAlias(string newAlias) {
            var data = new Dictionary<string, string>() {
                {"alias", newAlias }
            };
            client.SendCommand<SmartHomeResponse>("system", "set_dev_alias", data);
        }

        /// <summary>
        /// Sets the device's MAC address
        /// </summary>
        /// <param name="newMAC">New display name</param>
        public void SetMAC(string newMAC) {
            var data = new Dictionary<string, string>() {
                {"mac", newMAC }
            };
            client.SendCommand<SmartHomeResponse>("system", "set_mac_addr", data);
        }

        /// <summary>
        /// Sets the device's hardware ID
        /// </summary>
        /// <param name="newHardwareID">New display name</param>
        public void SetHardwareID(string newHardwareID) {
            var data = new Dictionary<string, string>() {
                {"hwId", newHardwareID }
            };
            client.SendCommand<SmartHomeResponse>("system", "set_hw_id", data);
        }

        public void SetLocation(float longitude, float latitude) {
            var data = new Dictionary<string, string>() {
                { "longitude", longitude.ToString() },
                { "latitude", latitude.ToString() }
            };
            client.SendCommand<SmartHomeResponse>("system", "set_dev_location", data);
        }

        /// <summary>
        /// Turns the LED on or off
        /// </summary>
        /// <param name="ledIsOn">True = LED is ON. False = LED is OFF.</param>
        public void SetLedState(bool ledIsOn) {
            var data = new Dictionary<string, string>() {
                { "off", (ledIsOn) ? "0" : "1" }
            };
            client.SendCommand<SmartHomeResponse>("system", "set_led_off", data);
        }

        /// <summary>
        /// Resets the device to factory settings. This cannot be undone.
        /// </summary>
        /// <param name="delay">Delay reset (Default is 1)</param>
        public void FactoryReset(int delay) {
            var data = new Dictionary<string, string>() {
                {"delay", delay.ToString() }
            };
            client.SendCommand<SmartHomeResponse>("system", "reset", data);
        }

        /// <summary>
        /// Perform uBoot Bootloader check.
        /// </summary>
        public void TestBootloader() {
            client.SendCommand<SmartHomeResponse>("system", "test_check_uboot");
        }

        /// <summary>
        /// Downloads firmware from the specified URL
        /// </summary>
        /// <param name="firmwareUrl">Full URL to the firmware bin file</param>
        public void DownloadFirmware(string firmwareUrl) {
            var data = new Dictionary<string, string>() {
                { "url" , firmwareUrl }
            };

            client.SendCommand<SmartHomeResponse>("system", "download_firmware", data);
        }

        /// <summary>
        /// Gets information about the state of downloads on the device
        /// </summary>
        /// <returns>Details about the state of downloads on the device.</returns>
        public SmartHomeDownloadState GetDownloadState() {
            return client.SendCommand<SmartHomeDownloadState>("system", "get_download_state");
        }

        /// <summary>
        /// Flashes the firmware downloaded using the DownloadFirmware() method.
        /// </summary>
        public void FlashFirmware() {
            client.SendCommand<SmartHomeResponse>("system", "flash_firmware");
        }

        /// <summary>
        /// Gets the current time as set on the smart home device.
        /// </summary>
        /// <returns>Current time set on the device</returns>
        public DateTime GetTime() {
            DeviceTime x = client.SendCommand<DeviceTime>("time", "get_time");
            return x.ToDateTime();
        }

        /// <summary>
        /// Gets the timezone index of the smart home device.
        /// </summary>
        /// <returns>Timezone index of the device</returns>
        public int GetTimezone() {
            var x = client.SendCommand<DeviceTimezone>("time", "get_timezone");
            return x.Index;
        }

        /// <summary>
        /// Updates the date and time set on the smart home device.
        /// Also updates the timezone index.
        /// This cannot be undone.
        /// </summary>
        /// <param name="newDate"></param>
        /// <param name="index"></param>
        public void SetTimezone(DateTime newDate, int index) {
            var data = new Dictionary<string, string>() {
                { "year", newDate.Year.ToString() },
                { "month", newDate.Month.ToString() },
                { "mday", newDate.Day.ToString() },
                { "hour", newDate.Hour.ToString() },
                { "min", newDate.Minute.ToString() },
                { "sec", newDate.Second.ToString() },
                { "index", index.ToString() }
            };

            client.SendCommand<SmartHomeResponse>("time", "set_timezone", data);
        }

        /// <summary>
        /// Read monthly stats for given year
        /// </summary>
        /// <param name="year">Year to retrieve stats for</param>
        public List<StatsMonth> ReadMonthStat(int year) {
            var data = new Dictionary<string, string>() {
                { "year", year.ToString() }
            };
            var x = client.SendCommand<SmartHomeStats>("emeter", "get_monthstat", data);
            return x.Data;
        }


        /// <summary>
        /// Read daily stats for a given month
        /// </summary>
        /// <param name="year">Year to retrieve stats for</param>
        /// <param name="month">Month to retrieve stats for</param>
        public List<StatsMonth> ReadDayStat(int year, int month) {
            var data = new Dictionary<string, string>() {
                { "year", year.ToString() },
                { "month", month.ToString() }
            };
            var x = client.SendCommand<SmartHomeStats>("emeter", "get_monthstat", data);
            return x.Data;
        }

        /// <summary>
        /// Read electricity meter details
        /// </summary>
        /// <returns>ElectricityReading object with details of voltage, current, and power.</returns>
        public ElectricityReading ReadElectricityMeter() {
            return client.SendCommand<ElectricityReading>("emeter", "get_realtime");
        }

        /// <summary>
        /// Reads the IGain and VGain settings for the plug.
        /// </summary>
        /// <returns></returns>
        public ElectricityGainSettings ReadElectricityGain() {
            return client.SendCommand<ElectricityGainSettings>("emeter", "get_vgain_igain");
        }

        /// <summary>
        /// Sets the IGain and VGain settings for the plug.
        /// </summary>
        /// <param name="gain"></param>
        public void SetElectricityGain(int vgain, int igain) {
            var data = new Dictionary<string, string>() {
                { "igain", igain.ToString() },
                { "vgain", vgain.ToString() }
            };

            client.SendCommand<SmartHomeResponse>("emeter", "set_vgain_igain", data);
        }

        /// <summary>
        /// Starts calibration of the plug
        /// </summary>
        /// <param name="vtarget">VTarget</param>
        /// <param name="itarget">ITarget</param>
        public void StartCalibration(int vtarget, int itarget) {
            var data = new Dictionary<string, string>() {
                { "vtarget", vtarget.ToString() },
                { "itarget", itarget.ToString() }
            };

            client.SendCommand<SmartHomeResponse>("emeter", "start_calibration", data);
        }

        /// <summary>
        /// Erases all electricity meter stats on the smart home device. This cannot be undone.
        /// </summary>
        public void EraseElectricityMeterStats() {
            client.SendCommand<SmartHomeResponse>("emeter", "erase_emeter_stat");
        }

        /// <summary>
        /// Turns the power on the plug on.
        /// </summary>
        public void EnablePower() => setPower(true);

        /// <summary>
        /// Turns the power on the plug off.
        /// </summary>
        public void DisablePower() => setPower(false);

        /// <summary>
        /// Toggles the power on based on the parameter.
        /// </summary>
        /// <param name="setPowerOn">True = Turn ON power. False = Turn OFF power.</param>
        private void setPower(bool setPowerOn) {
            var powerState = (setPowerOn) ? "1" : "0";
            var data = new Dictionary<string, string>() { { "state", powerState } };
            client.SendCommand<SmartHomeResponse>("system", "set_relay_state", data);
        }

        /// <summary>
        /// Returns details about the device's connection to the Kasa cloud service.
        /// </summary>
        /// <returns>Details about the device's connection to the Kasa cloud service.</returns>
        public SmartHomeCloudInfo GetCloudInfo() {
            return client.SendCommand<SmartHomeCloudInfo>("cnCloud", "get_info");
        }

        /// <summary>
        /// Get list of firmware available for download.
        /// </summary>
        /// <returns>An enumeration of firmware details that are avaiulable for download.</returns>
        public IEnumerable<SmartHomeFirmwareInfo> GetFirmwareList() {
            var r = client.SendCommand<SmartHomeFirmwareList>("cnCloud", "get_intl_fw_list");
            return r?.Firmware;
        }

        /// <summary>
        /// Sets the hostname to use when contacting the Kasa cloud service.
        /// </summary>
        /// <param name="hostname">Hostname to use for the cloud service.</param>
        public void SetCloudServer(string hostname) {
            var data = new Dictionary<string, string>() {
                { "server" , hostname }
            };

            client.SendCommand<SmartHomeResponse>("cnCloud", "set_server_url", data);
        }

        /// <summary>
        /// Sets the username and password to use when authenticating to the Kasa cloud service.
        /// </summary>
        /// <param name="username">Kasa username. Usually your email.</param>
        /// <param name="password">Password for the Kasa cloud service account.</param>
        public void SetCloudAuth(string username, string password) {
            var data = new Dictionary<string, string>() {
                { "username", username },
                { "password", password }
            };

            client.SendCommand<SmartHomeResponse>("cnCloud", "bind", data);
        }

        /// <summary>
        /// Unregisters the device from the Kasa cloud service.
        /// </summary>
        public void UnregisterCloud() {
            client.SendCommand<SmartHomeResponse>("cnCloud", "unbind");
        }

        /// <summary>
        /// Checks for available wifi networks.
        /// </summary>
        /// <returns>Collection of Wifi networks.</returns>
        public IEnumerable<SmartHomeWifiNetwork> GetWifiNetworks() {
            var data = new Dictionary<string, string>() {
                {  "refresh", "1" }
            };
            var r = client.SendCommand<SmartHomeWifiList>("netif", "get_scaninfo", data);
            return r?.Networks;
        }

        /// <summary>
        /// Sets the Wifi network that the device should use.
        /// </summary>
        /// <param name="ssid">SSID of the network</param>
        /// <param name="password">Encryption password for the network. If null or empty no encryption will be used.</param>
        public void SetWifiNetwork(string ssid, string password) {
            var data = new Dictionary<string, string>() {
                { "ssid", ssid }
            };

            if (string.IsNullOrEmpty(password)) {
                data.Add("key_type", "0");
            }
            else {
                data.Add("key_type", "3");
                data.Add("password", password);
            }

            client.SendCommand<SmartHomeResponse>("netif", "", data);
        }
    }
}