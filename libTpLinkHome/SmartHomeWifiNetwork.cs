﻿using System;
using System.Collections.Generic;
using System.Text;
using Newtonsoft.Json;

namespace EonDream.SmartHome.TpLink {
    public class SmartHomeWifiNetwork {
        public string Ssid { get; set; }
        [JsonProperty(PropertyName = "key_type")]
        public int KeyType { get; set; }
        public WifiNetworkEncryption EncryptionType {
            get {
                // additional wifi encryption types will be added when i work out the value mapping
                switch (KeyType) {
                    case 0:
                        return WifiNetworkEncryption.None;
                    case 3:
                        return WifiNetworkEncryption.WPA2;
                    default:
                        return WifiNetworkEncryption.Unknown;
                }
            }
        }
    }
}
