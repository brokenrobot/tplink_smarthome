﻿using System;
using System.Collections.Generic;
using System.Text;
using Newtonsoft.Json;

namespace EonDream.SmartHome.TpLink {
    public class SmartHomeFirmwareInfo {
        [JsonProperty(PropertyName = "fwReleaseLogUrl")]
        public string ReleaseLogUrl { get; set; }

        [JsonProperty(PropertyName = "fwLocation")]
        public int Location { get; set; }

        [JsonProperty(PropertyName = "fwVer")]
        public string Version { get; set; }

        [JsonProperty(PropertyName = "fwReleaseLog")]
        public string ReleaseLog { get; set; }

        [JsonProperty(PropertyName = "fwTitle")]
        public string Title { get; set; }

        [JsonProperty(PropertyName = "fwReleaseDate")]
        public DateTime ReleaseDate { get; set; }

        [JsonProperty("fwUrl")]
        public string DownloadUrl { get; set; }
    }
}
