﻿using System;
using System.Collections.Generic;
using System.Text;
using Newtonsoft.Json;

namespace EonDream.SmartHome.TpLink {
    class DeviceTime : SmartHomeResponse {
        public int Year { get; set; }
        public int Month { get; set; }
        [JsonProperty(PropertyName = "mday")]
        public int Day { get; set; }
        public int Hour { get; set; }
        [JsonProperty(PropertyName = "min")]
        public int Minute { get; set; }
        [JsonProperty(PropertyName = "sec")]
        public int Second { get; set; }
        public int Index { get; set; }

        public DeviceTime(DateTime dt) {
            Year = dt.Year;
            Month = dt.Month;
            Day = dt.Day;
            Hour = dt.Hour;
            Minute = dt.Minute;
            Second = dt.Second;
        }

        public DateTime ToDateTime() {
            return new DateTime(Year, Month, Day, Hour, Minute, Second);
        }
    }
}
