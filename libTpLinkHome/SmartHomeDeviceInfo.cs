﻿using System;
using System.Collections.Generic;
using System.Text;
using Newtonsoft.Json;

namespace EonDream.SmartHome.TpLink {
    public class SmartHomeDeviceInfo : SmartHomeResponse {
        [JsonProperty(PropertyName = "sw_ver")]
        public string SoftwareVersion { get; set; }

        [JsonProperty(PropertyName = "hw_ver")]
        public string HardwareVersion { get; set; }

        public string Type { get; set; }

        public string Model { get; set; }

        public string MAC { get; set; }

        [JsonProperty(PropertyName = "dev_name")]
        public string DeviceName { get; set; }

        public string Alias { get; set; }

        [JsonProperty(PropertyName = "relay_state")]
        public bool IsRelay { get; set; }

        [JsonProperty(PropertyName = "on_time")]
        public int OnTime { get; set; }

        [JsonProperty(PropertyName = "active_mode")]
        public string ActiveMode { get; set; }

        public string Feature { get; set; }

        [JsonProperty(PropertyName = "updating")]
        public bool IsUpdating { get; set; }

        public int RSSI { get; set; }

        [JsonProperty(PropertyName = "led_off")]
        public bool IsLedOff { get; set; }

        [JsonProperty(PropertyName = "longitude_i")]
        public string Longitude { get; set; }

        [JsonProperty(PropertyName = "latitude_i")]
        public string Latitude { get; set; }

        [JsonProperty(PropertyName = "hwId")]
        public string HardwareId { get; set; }

        public string OemId { get; set; }

        [JsonProperty(PropertyName = "fwId")]
        public string FirmwareId { get; set; }

        public string DeviceId { get; set; }
    }
}
