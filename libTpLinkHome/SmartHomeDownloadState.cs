﻿using System;
using System.Collections.Generic;
using System.Text;
using Newtonsoft.Json;

namespace EonDream.SmartHome.TpLink {
    public class SmartHomeDownloadState : SmartHomeResponse {
        public int Status { get; set; }
        public int Ratio { get; set; }
        [JsonProperty(PropertyName = "reboot_time")]
        public int RebootTime { get; set; }
        [JsonProperty(PropertyName = "flash_time")]
        public int FlashTime { get; set; }
    }
}
